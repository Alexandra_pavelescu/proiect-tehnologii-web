import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import {
  Route,
  NavLink,
  HashRouter
} from "react-router-dom";
import Companii from "./Components/Companii";
import Cupoane from "./Components/Cupoane";
import Locatii from "./Components/Locatii";
import Login from "./Components/Login/Login";
import Register from "./Components/Register/Register";


class App extends Component {
  render() {
    return (
      <HashRouter>
      <div className="App">
         <h1>Cuponel.ro</h1>
          <ul className="header">
            <li><NavLink to="/cupoane">Cupoane</NavLink></li>
            <li><NavLink to="/companii">Companii</NavLink></li>
            <li><NavLink to="/locatii">Locatii</NavLink></li>
            <li><NavLink to="/login">Login/Logout</NavLink></li>

          </ul>
          <div className="content">
             <Route path="/cupoane" component={Cupoane}/>
             <Route path="/companii" component={Companii}/>
             <Route path="/locatii" component={Locatii}/>
             <Route path="/login" component={Login}/>
             <Route path="/register" component={Register}/>

          </div>
     
      </div>
      </HashRouter>
    );
  }
}

export default App;
