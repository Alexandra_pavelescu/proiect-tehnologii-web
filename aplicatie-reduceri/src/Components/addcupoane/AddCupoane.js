import React from "react";
import axios from 'axios';
import './AddCupoane.css';
 
class AddCupoane extends React.Component {
  
  constructor(props){
    super(props);
    this.state={};
    this.state.discount=0;
    this.state.unitateDiscount="";
    this.state.dataInceput="";
    this.state.dataExpirare="";
    this.state.idCompanie=1;
    this.state.username="lnita";
  }
  
   handleChangeCompanie = (event)=>{
    this.setState({
            idCompanie: event.target.value
        })
  }
  
  handleChangeDiscount = (event)=>{
    this.setState({
            discount: event.target.value
        })
  }
  
  handleChangeUnitateDiscount = (event)=>{
    this.setState({
            unitateDiscount: event.target.value
        })
  }
  
  handleChangeDataInceput = (event)=>{
    this.setState({
            dataInceput: event.target.value
        })
  }
  
  handleChangeDataExpirare = (event)=>{
    this.setState({
            dataExpirare: event.target.value
        })
  }
  
   handleAddClick = () => {
        let cupon = {
          dataExpirare: this.state.dataExpirare,
          discount: this.state.discount,
          unitateDiscount: this.state.unitateDiscount,
          dataInceput: this.state.dataInceput,
            //username: this.state.username,
            //idCompanie: this.state.idCompanie
            
        }
        axios.post('https://proiect-tw-alexandrapavelescu.c9users.io:8081/adauga-cupon', cupon).then((res) => {
            if(res.status === 200){
                this.props.cuponAdded(cupon);
            }
        }).catch((err) =>{
            console.log(err);
        });
    }
  
  render() {
    return (
      <div>
        <h2>Adauga cupon nou</h2>
        <h3><div>
        <pre>  Discount                  </pre><pre>Unitate  </pre><pre>Data de inceput     </pre><pre>Data de expirare</pre>
        </div></h3>
        <input type="number" value={this.state.discount} onChange={this.handleChangeDiscount}/>
        <select value={this.state.unitateDiscount} onChange={this.handleChangeUnitateDiscount}>
        <option value="lei">lei</option>
        <option value="euro">euro</option>
        <option value="%">%</option>
        </select>
        <input type="date" value={this.state.dataInceput} onChange={this.handleChangeDataInceput} label="Data de inceput"/>
        <input type="date" value={this.state.dataExpirare} onChange={this.handleChangeDataExpirare} label="Data de expirare"/>
        <button  onClick={this.handleAddClick}>Adauga cupon</button>
      </div>
    );
  }
}
 
export default AddCupoane;