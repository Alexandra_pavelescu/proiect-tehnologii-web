import React from 'react';
import './ListaCompanii.css';

import { Link } from 'react-router-dom'


export class ListaCompanii extends React.Component{
    
    constructor(props){
        super(props);
        
    }
    
    
    render(){
        let items = this.props.source.map((companie, index) =>{
            return <div key={index} className="companieAfisata">
                <p className="denumireCompanieAfisata"> {companie.idCompanie}. {companie.denumire}</p>
                <p className="tipCompanieAfisata"> {companie.tipCompanie}</p>
            </div>
        })
        return(
            <div>
                <h1>{this.props.title}</h1>
                <div>
                {items}
                </div>
            </div>
            );
    }
}