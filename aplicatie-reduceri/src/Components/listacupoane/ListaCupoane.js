import React from 'react';


 class ListaCupoane extends React.Component {
    // eslint-disable-next-line
    constructor(props)
    {
        super(props);
    
    }


    
    render(){
        let items = this.props.source.map((cupon, index) =>{
            return <div key={index} className="companieAfisata">
            <p className="denumireCompanieAfisata">Discount: {cupon.discount} {cupon.unitateDiscount}</p>
            <p className="tipCompanieAfisata">, valabil de la: {cupon.dataInceput} pana la: {cupon.dataExpirare}</p>
            </div>
            
        });
        
    
        return(
            <div>
                <h2>{this.props.title}</h2>
                <div>
                {items}
                </div>
            </div>
            );
    }
}

export default ListaCupoane;