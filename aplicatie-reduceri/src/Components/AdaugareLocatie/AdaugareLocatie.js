import React from 'react';
import axios from 'axios';

export class AdaugareLocatie extends React.Component{
    
    
     constructor(props){
        super(props);
        this.state = {};
        this.state.idCompanie = 12;
        this.state.adresa="";
    }
    
    handleSchimbareAdresa = (event) => {
        this.setState({
            adresa: event.target.value
        })
    }
    
    onSchimbareIdCompanie = (idCompanie) => {
        this.setState({
            idCompanie: idCompanie
        })
    }
    
    handleAdaugareLocatieClick = () => {
        let locatie = {
            adresa: this.state.adresa,
        }
        axios.post('https://proiect-tw-alexandrapavelescu.c9users.io:8081/companii/'+this.state.idCompanie+'/locatii/', locatie).then((res) => {
            if(res.status === 200){
                //this.props.companieAdaugata(companie)
            }
        }).catch((err) =>{
            console.log(err)
        })
    }
    
    render(){
        return(
            <div>
                <h3 className="subtitlu1">Adauga o locatie pentru compania selectata: </h3>
                <input type="text" placeholder="adresa" onChange={this.handleSchimbareAdresa}/>
                <button onClick={this.handleAdaugareLocatieClick}>Adauga</button>
            </div>
            );
    }
}