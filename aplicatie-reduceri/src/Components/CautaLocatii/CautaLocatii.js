import React from 'react';
import axios from 'axios';
import './CautaLocatii.css';

export class CautaLocatii extends React.Component{
    
     constructor(props){
        super(props);
        this.state = {};
        this.state.denumire = "";
        this.state.adresa="";
        this.state.locatii = [];
    }
    
     handleSchimbareCompanie = (event) => {
        this.setState({
            denumire: event.target.value
        })
    }
    
     handleSchimbareAdresa = (event) => {
        this.setState({
            adresa: event.target.value
        })
    }
    
    handleCautareLocatiiClick = () => {
        let companie=this.state.denumire;
        //const url = 'https://proiect-tw-alexandrapavelescu.c9users.io:8081/locatii';
        const url = 'https://proiect-tw-alexandrapavelescu.c9users.io:8081/companii/'+companie+'/locatii';
        fetch(url).then((res) => {
          return res.json();
        }).then((locatii) =>{
          this.setState({
            locatii: locatii
          });
          this.props.locatiiCautate(locatii)
        })
    }
    
    handleAdaugareLocatieClick = () => {
        let locatie = {
            adresa: this.state.adresa,
        }
        axios.post('https://proiect-tw-alexandrapavelescu.c9users.io:8081/companii/'+this.state.denumire+'/locatii/', locatie).then((res) => {
            if(res.status === 200){
                this.props.locatieAdaugata(locatie)
            }
        }).catch((err) =>{
            console.log(err)
        })
    }
    
    
    render(){
        return(
            <div>
                <div>
                <h3 className="subtitlu1">Cauta locatii pentru compania: </h3>
                <input type="text" placeholder="id companie" onChange={this.handleSchimbareCompanie} value={this.state.denumire}/>
                <button class="blogin" onClick={this.handleCautareLocatiiClick}>Cauta</button>
                </div>
                
                <div>
                <h3 className="subtitlu1">Adauga o locatie pentru compania selectata: </h3>
                <input type="text" placeholder="adresa" onChange={this.handleSchimbareAdresa}/>
                <button class="blogin" onClick={this.handleAdaugareLocatieClick}>Adauga</button>
                </div>
            </div>
            );
    }
}