/*import React, { Component } from "react";
 
class Cupoane extends Component {
  render() {
    return (
      <div>
        <h2>CUPOANE</h2>
        <p>Cras facilisis urna ornare ex volutpat, et
        convallis erat elementum. Ut aliquam, ipsum vitae
        gravida suscipit, metus dui bibendum est, eget rhoncus nibh
        metus nec massa. Maecenas hendrerit laoreet augue
        nec molestie. Cum sociis natoque penatibus et magnis
        dis parturient montes, nascetur ridiculus mus.</p>
 
        <p>Duis a turpis sed lacus dapibus elementum sed eu lectus.</p>
      </div>
    );
  }
}
 
export default Cupoane;*/


import React, { Component } from 'react';


import AddCupoane from './addcupoane/AddCupoane';
import ListaCupoane from './listacupoane/ListaCupoane';

class Cupoane extends Component {
  
  constructor(props){
    super(props);
    this.state = {};
    this.state.cupoane = [];
    
  }
  
  oncuponAdded = (cupon) => {
    let cupoane = this.state.cupoane;
    cupoane.push(cupon);
    this.setState({
      cupoane: cupoane
    });
  }
  
  componentWillMount(){
    const url = 'https://proiect-tw-alexandrapavelescu.c9users.io:8081/toate-cupoanele';
    fetch(url).then((res) => {
      return res.json();
    }).then((cupoane) =>{
      this.setState({
        cupoane: cupoane
      })
    })
  }
  render() {
    return (
      <React.Fragment>
        <AddCupoane cuponAdded={this.oncuponAdded}/>
        <ListaCupoane title="Lista cupoane" source={this.state.cupoane}/>
      </React.Fragment>
    );
  }
}

export default Cupoane;

