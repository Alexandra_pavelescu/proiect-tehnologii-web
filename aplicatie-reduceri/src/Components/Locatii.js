import React, { Component } from "react";

import {CautaLocatii} from './CautaLocatii/CautaLocatii';
import {LocatiiCompanie} from './LocatiiCompanie/LocatiiCompanie';
import {AdaugareLocatie} from './AdaugareLocatie/AdaugareLocatie';
 
class Locatii extends Component {
    
    constructor(props){
    super(props);
    this.state = {};
    this.state.locatii = [];
  }
  
  onLocatieCautata = (locatii) => {
      this.setState({
        locatii: locatii
      })
  }
  
  onLocatieAdaugata = (locatie) => {
      let locatii = this.state.locatii;
      locatii.push(locatie);
      this.setState({
      locatii: locatii
    });
  }
  
  
  render() {
    return (
      <div>
        <h2>LOCATII</h2>
        <React.Fragment>
          <CautaLocatii  locatiiCautate={this.onLocatieCautata} locatieAdaugata={this.onLocatieAdaugata}/>
          <LocatiiCompanie source={this.state.locatii}/>
         </React.Fragment>
      </div>
    );
  }
}
 
export default Locatii;