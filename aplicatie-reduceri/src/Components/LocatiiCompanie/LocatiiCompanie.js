import React from 'react';
import './LocatiiCompanie.css';


export class LocatiiCompanie extends React.Component{
    
    constructor(props){
        super(props);
        
    }
    
    
    render(){
        let items = this.props.source.map((locatie, index) =>{
            return <div key={index} className="companieAfisata">
                <p className="denumireCompanieAfisata"> {locatie.adresa}</p>
            </div>
        })
        return(
            <div>
                <h1>{this.props.title}</h1>
                <div>
                {items}
                </div>
            </div>
            );
    }
}