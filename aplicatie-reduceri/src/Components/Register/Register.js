import React from 'react';
import axios from 'axios';

import {Link} from "react-router-dom";

export default class Register extends React.Component {
 constructor(props){
        super(props);
        this.state = {};
        this.state.nume="";
        this.state.prenume="";
        this.state.email="";
        this.state.username="";
        this.state.parola="";
    }
    
     handleNume = (ev) => 
        { this.setState(
        {nume : ev.target.value}
        )};
        
    handlePrenume = (ev) => 
        { this.setState(
        {prenume : ev.target.value}
        )};
    
    handleEmail = (ev) => 
        { this.setState(
        {email : ev.target.value}
        )};
    handleUserName = (ev) => 
        { this.setState(
        {username : ev.target.value}
        )};
        
    handleParola = (ev) => 
        { this.setState(
        {parola : ev.target.value}
        )};
        
   
        
        
    validate()
    {
        return this.state.username.length > 0 && this.state.parola.length > 6
        && this.state.nume.length > 0 && this.state.prenume.length > 0 && this.state.email.length > 0;
    }
   
   
    onClick = (ev) => {
        ev.preventDefault();
        let utilizator = {
            nume : this.state.nume,
            prenume: this.state.prenume,
            email : this.state.email,
            username : this.state.username,
            parola : this.state.parola
        }
        axios.post('https://proiect-tw-alexandrapavelescu.c9users.io:8081/inregistrare', utilizator).then((res) => {
            if(res.status === 200){
               console.log("success");
            }
        }).catch((err) =>{
            console.log(err)
        });
    }
        render(){
        
        return(
           
            <div>
            <p>Formular de inregistrare</p>
            
            <p>Nume*</p>
            <input type="text" class="ilogin"
            value={this.state.nume}
            onChange={this.handleNume}/>
            
            <p>Prenume*</p>
            <input type="text" class="ilogin"
            value={this.state.prenume}
            onChange={this.handlePrenume}/>
            
            <p>Adresa e-mail*</p>
            <input type="email" class="ilogin"
            value={this.state.email}
            onChange={this.handleEmail}/>
            
            <p>Nume de utilizator*</p>
            <input type="text" class="ilogin"
            value={this.state.username}
            onChange={this.handleUserName}/>
            
            <p>Parola* <i>(trebuie sa contina minim 6 caractere)</i></p>
            <input type="password" class="ilogin"
            value={this.state.parola}
            onChange={this.handleParola}/>
            
            <p></p>
            
            <button class="blogin" onClick={this.onClick} disabled={!this.validate()}>Inregistrare</button>
            
            <Link to="login">Inapoi</Link>
            
            <p>*Campuri obligatorii</p>
            </div>
            
            
            );
        
    }
    
    
    
    
}

