import React, { Component } from "react";

import {AdaugareCompanie} from './AdaugareCompanie/AdaugareCompanie';
import {ListaCompanii} from './ListaCompanii/ListaCompanii';
 
class Companii extends Component {
  
  constructor(props){
    super(props);
    this.state = {};
    this.state.companii = [];
  }
  
  onCompanieAdaugata = (companie) => {
    let companii = this.state.companii;
    let companie2 = {
      idCompanie: companii.length+11,
      denumire: companie.denumire,
      tipCompanie: companie.tipCompanie
    }
    companii.push(companie2);
    this.setState({
      companii: companii
    });
  }
  
   componentWillMount(){
    const url = 'https://proiect-tw-alexandrapavelescu.c9users.io:8081/companii'
    fetch(url).then((res) => {
      return res.json();
    }).then((companii) =>{
      this.setState({
        companii: companii
      })
    })
  }
  
  render() {
    return (
      <div>
        <h2>COMPANII</h2>
        
         <React.Fragment>
          <AdaugareCompanie companieAdaugata={this.onCompanieAdaugata} />
          <ListaCompanii  source={this.state.companii} />
         </React.Fragment>
      </div>
    );
  }
}
 
export default Companii;