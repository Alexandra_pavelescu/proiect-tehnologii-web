import React from 'react';

import {Link,HashRouter} from "react-router-dom";


export default class Login extends React.Component {
    constructor(props){
        super(props);
        this.state = {};
        this.state.username="";
        this.state.parola="";
    }
    
    handleUserName = (ev) => 
        { this.setState(
        {username : ev.target.value}
        )};
        
    handleParola = (ev) => 
        { this.setState(
        {parola : ev.target.value}
        )};
    
    validate()
    {
        return this.state.username.length > 0 && this.state.parola.length > 6;
    }
    
    
    handleClickLogin = (ev) =>
    {
        ev.preventDefault();
        /*let user = {
            username : this.state.username,
            parola : this.state.parola
        };*/
       
       //alert("Utilizatorul "+this.state.username+" s-a logat");
    }
   
   handleClickInregistrare()
   {
       
   }

   
    
    render(){
        
        return(
            <HashRouter>
            <div>
            <h2>Login</h2>
            <p>Nume de utilizator</p>
            <input type="text" class="ilogin"
            value={this.state.username}
            onChange={this.handleUserName}/>
            
            <p>Parola <i>(trebuie sa contina minim 6 caractere)</i></p>
            <input type="password" class="ilogin"
            value={this.state.parola}
            onChange={this.handleParola}/>
            
            <Link to="/cupoane"><button class="blogin"
            disabled={!this.validate()}>Login</button></Link>
            
            <p>Nu ai cont? Inregistreaza-te! </p>
             <Link to="/register"><button class="blogin">Inregistrare</button></Link>
    
            </div>
            
            </HashRouter>
            );
        
    }
    
    
    
}

