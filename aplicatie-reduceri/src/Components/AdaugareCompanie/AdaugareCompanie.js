import React from 'react';
import axios from 'axios';
import './AdaugareCompanie.css';

export class AdaugareCompanie extends React.Component{
    
    constructor(props){
        super(props);
        this.state = {};
        this.state.denumire = "";
        this.state.tipCompanie = "altceva";
    }
    
     handleSchimbareDenumire = (event) => {
        this.setState({
            denumire: event.target.value
        })
    }
    
     handleSchimbareTipCompanie = (event) => {
        this.setState({
            tipCompanie: event.target.value
        })
    }
    
    handleAdaugareCompanieClick = () => {
        let companie = {
            denumire: this.state.denumire,
            tipCompanie: this.state.tipCompanie
        }
        //let deTrimis = [];
        //deTrimis.push(companie);
        axios.post('https://proiect-tw-alexandrapavelescu.c9users.io:8081/adaugare-companie', companie).then((res) => {
            if(res.status === 200){
                this.props.companieAdaugata(companie)
            }
        }).catch((err) =>{
            console.log(err)
        })
    }
    
    render(){
        return(
                <div>
                    <h3 className="subtitlu1">Adauga o companie: </h3>
                    <input type="text" placeholder="denumire companie" onChange={this.handleSchimbareDenumire} value={this.state.denumire}/>
                    <select name="tipcompanie" onChange={this.handleSchimbareTipCompanie} value={this.state.tipCompanie}>
                         <option value="magazin">Magazin</option>
                         <option value="restaurant">Restaurant</option>
                         <option value="cinema">Cinema</option>
                         <option value="supermarket">Supermarket</option>
                         <option value="pub">Pub</option>
                         <option value="club">Club</option>
                         <option value="cafenea">Cafenea</option>
                         <option value="altceva">Altceva</option>
                    </select>
                    <button class="blogin" onClick={this.handleAdaugareCompanieClick}>Adauga</button>
                </div>
            );
    }
}