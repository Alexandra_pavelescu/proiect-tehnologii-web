###Tema 27 - Aplicatie pentru gestionarea cupoanelor de reduceri cu integrare Foursquare/Google Places

Functionalitati:

1. Functie de creare cont (inregistrare pe baza de nume complet, data nasterii, locatie si adresa de e-mail) + autentificare pe baza de username si parola

2. Fiecare utilizator are posibilitatea de a inregistra toate cupoanele de reducere pe care le detine, o inregistrare continand: 
	- Numele magazinului sau localului
	- Produsul/serviciul supus ofertei
	- Locatiile pentru care este valabila reducerea
	- Data/perioada de valabilitate
	- Suma/procentul de reducere
	- Suma minima pentru care se aplica reducerea
	- De cate ori/persoane poate fi folosit
	- Alte detalii referitoare la cupon
	- Starea cuponului: valabil/expirat/utilizat
	
3. De asemenea, utilizatorul poate vedea toate cupoanele de reducere pe care le detine, le poate filtra dupa mai multe criterii (locatie, magazin, stare, etc) si le poate edita

4. Gestionarea starilor cuponului:
	- In momentul in care perioada de valabilitate a unui cupon expira, starea acestuia va fi schimbata automat in expirat
	- In momentul in care cuponul a fost folosit, starea acestuia se va schimba in utilizat
	- In momentul in care este introdus un cupon, starea default a acestuia este "valabil"
	
5. Aplicatia se va folosi de API-ul Foursquare pentru localizarea geografica a localurilor/magazinelor

6. Utilizatorul poate introduce in bara de cautare o locatie pentru a vedea toate cupoanele valabile in locatia respectiva si in imprejurimi

7. Fiecare utilizator detine un profil public care contine: 
	- Poza de profil
	- Descriere
	- Numarul de cupoane detinute
	- Prieteni
	
8. Fiecare utilizator are posibilitatea de a-si gestiona contul, avand optiunile:
	- Vizualizare profil
	- Editare date de contact
	- Schimbare parola
	- Vizualizare, editare, stergere si adaugare cupoane in colectia proprie
	- Ordonarea cupoanelor dupa valabilitate sau procentul reducerii
	- Prioritizarea anumitor cupoane
	- Setarea unor locatii (orase, magazine, restaurante, etc) favorite
	- Istoricul cupoanelor utilizate si locatiilor vizitate
	- Setarea unei notificari in momentul in care cuponul este aproape de data expirarii
	- "calculator" economii + afisare total economii
	
*9. Posibilitatea de a trimite cupoane de reducere prietenilor/de a primi cupoane de reducere de la prieteni