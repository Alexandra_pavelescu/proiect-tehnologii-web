const express = require('express');
const bodyParser = require('body-parser');
const Sequelize = require('sequelize');
const cors = require('cors');

const app = express();
app.use(cors());
app.use(bodyParser.json());


const sequelize = new Sequelize('c9', 'alexandrapaveles', '', {
   host: 'localhost',
   dialect: 'mysql',
   operatorsAliases: false,
   pool: {
        "max": 1,
        "min": 0,
        "idle": 20000,
        "acquire": 20000
    }
});

sequelize
  .authenticate()
  .then(() => {
    console.log('Conexiunea s-a realizat cu succes!');
  })
  .catch(err => {
    console.error('Nu s-a putut realiza conexiunea la baza de date', err);
  });
  
  
  //tabela Utilizator
  const Utilizator = sequelize.define('utilizatori', {
   nume: {
       type: Sequelize.STRING,
       allowNull: false
   }, 
   prenume: {
        type: Sequelize.STRING,
        allowNull: false
   },
   email: {
       type: Sequelize.STRING,
       allowNull: false
   },
   username: {
       type: Sequelize.STRING,
       allowNull: false,
       primaryKey: true
   }, 
   parola: {
       type: Sequelize.STRING,
       allowNull: false
   }
});

//tabela companie
const Companie = sequelize.define('companii', {
    idCompanie: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    denumire: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    tipCompanie: {
        type: Sequelize.STRING,
        allowNull: false
    }
});

//tabela locatii
const Locatie = sequelize.define('locatii', {
    idLocatie: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    adresa: {
        type: Sequelize.STRING, 
        allowNull: false
    }
});


//tabela cupon
const Cupon = sequelize.define('cupoane', {
    idCupon: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    status: {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: 'neutilizat'
    },
    dataInceput: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW
    },
    dataExpirare: {
         type: Sequelize.DATE,
         allowNull: false
    },
    discount: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    unitateDiscount: {
        type: Sequelize.STRING,
        allowNull: false
    },
    observatii: {
        type: Sequelize.STRING
    }
});


Companie.hasMany(Locatie, {foreignKey: 'idCompanie'});
Locatie.belongsTo(Companie, {foreignKey: 'idCompanie'});
Utilizator.hasMany(Cupon, {foreignKey: 'username'});
Cupon.belongsTo(Utilizator,{foreignKey: 'username'});
Companie.hasMany(Cupon, {foreignKey: 'idCompanie'});
Cupon.belongsTo(Companie, {foreignKey: 'idCompanie'});



sequelize.sync({force: true}).then(()=>{
    console.log('Baza de date a fost creata cu succes!');
    
    
    //adaugare utilizatori
Utilizator.create({
    nume: 'Popescu',
    prenume: 'Andrei',
    email: 'apopescu@gmail.com',
    username: 'apopescu',
    parola: 'parolapopescu'
})
.then(utilizatorNou => {
  console.log(`Utilizator nou:  ${utilizatorNou.username}, cu parola ${utilizatorNou.parola} a fost creat.`);
});

Utilizator.create({
    nume: 'Geogescu',
    prenume: 'Maria',
    email: 'mgeorgescu@gmail.com',
    username: 'mgeorgescu',
    parola: 'parolageorgescu'
})
.then(utilizatorNou => {
  console.log(`Utilizator nou:  ${utilizatorNou.username}, cu parola ${utilizatorNou.parola} a fost creat.`);
});

Utilizator.create({
    nume: 'Nita',
    prenume: 'Lucretia',
    email: 'lnita@gmail.com',
    username: 'lnita',
    parola: 'parolanita'
})
.then(utilizatorNou => {
  console.log(`Utilizator nou:  ${utilizatorNou.username}, cu parola ${utilizatorNou.parola} a fost creat.`);
});



Companie.create({
    idCompanie:"11",
    denumire:"comp1",
    tipCompanie:"srl"
}).then(companieNoua=>{
    console.log(`Companie noua: ${companieNoua.idCompanie}, ${companieNoua.denumire} a fost creata.`);
});

Companie.create({
    idCompanie:"12",
    denumire:"comp2",
    tipCompanie:"srl"
}).then(companieNoua=>{
    console.log(`Companie noua: ${companieNoua.idCompanie}, ${companieNoua.denumire} a fost creata.`);
});

Locatie.create({
    idLocatie:"101",
    adresa:"sector 1, Bucuresti",
    idCompanie:"11"
}).then(locatieNoua=>{
    console.log(`Locatie noua: ${locatieNoua.idLocatie}, ${locatieNoua.adresa}, ${locatieNoua.companiiIdCompanie} a fost creata.`);
});

Locatie.create({
    idLocatie:"102",
    adresa:"sector 2, Bucuresti",
    idCompanie:"11"
}).then(locatieNoua=>{
    console.log(`Locatie noua: ${locatieNoua.idLocatie}, ${locatieNoua.adresa}, ${locatieNoua.companiiIdCompanie} a fost creata.`);
});

Locatie.create({
    idLocatie:"103",
    adresa:"sector 1, Bucuresti",
    idCompanie:"12"
}).then(locatieNoua=>{
    console.log(`Locatie noua: ${locatieNoua.idLocatie}, ${locatieNoua.adresa} a fost creata.`);
});

Cupon.create({
    idCupon:"1001",
    status:"neutilizat",
    dataExpirare:"2019-01-05T08:00:00.000Z",
    discount:"10",
    unitateDiscount:"%",
    observatii:"nu",
    username:"lnita",
    idCompanie:"11"
}).then(cuponNou=>{
    console.log(`Cupon nou: ${cuponNou.idCupon} a fost creat.`);
});

Cupon.create({
    idCupon:"1002",
    status:"neutilizat",
    dataExpirare:"2019-01-05T08:00:00.000Z",
    discount:"15",
    unitateDiscount:"%",
    observatii:"nu",
    username:"lnita",
    idCompanie:"12"
}).then(cuponNou=>{
    console.log(`Cupon nou: ${cuponNou.idCupon} a fost creat.`);
});

Cupon.create({
    idCupon:"1003",
    status:"neutilizat",
    dataExpirare:"2019-01-05T08:00:00.000Z",
    discount:"10",
    unitateDiscount:"%",
    observatii:"nu",
    username:"apopescu",
    idCompanie:"11"
}).then(cuponNou=>{
    console.log(`Cupon nou: ${cuponNou.idCupon} a fost creat.`);
});

});

//Metode tabela Utilizator

//inregistrare utilizator
app.post('/inregistrare', (req, res) =>{
    Utilizator.create({
        nume: req.body.nume,
        prenume: req.body.prenume,
        email: req.body.email,
        username: req.body.username,
        parola: req.body.parola
    }).then((user) => {
        res.status(200).send("Utilizatorul a fost creat cu succes!");
    }, (err) => {
        res.status(500).send(err);
    })
});


//login utilizator
app.post('/login', (req, res) => {
   Utilizator.findOne({where:{username: req.body.username, parola: req.body.parola} }).then((result) => {
       res.status(200).send(result)
   }) 
});

//afisare utilizatori
app.get('/toti-utilizatorii', (req, res) => {
    Utilizator.findAll().then((utilizatori) => {
        res.status(200).send(utilizatori); 
    });
});

//stergere utilizator
app.delete('/stergere/:username', (req, res) => {
    const username = req.params.username;
    const parola = req.body.parola;
    Utilizator.destroy({
       where: { username: username,
                parola: parola
       }
    }).then((user) => {
        res.status(200).send("Utilizatorul a fost sters cu succes!");
    }, (err) => {
        res.status(500).send(err);
    });
});


//actualizare utilizator
app.put('/utilizatori/actualizare/:username', (req, res) => {
     const username = req.params.username;
     const parola = req.body.parola;
     Utilizator.update(
         {
             nume: req.body.nume,
             prenume: req.body.prenume,
             email: req.body.email,
             parola: req.body.parolaNoua
         },
         {
             where: {
                 username: username,
                 parola: parola
             }
         })
         .then((user) => {
             res.status(200).send("Utilizatorul a fost actualizat cu succes!");
         }, (err) => {
            res.status(500).send(err);
         });
});

//Metode pentru tabela CUPON

//creare cupon
app.post('/adauga-cupon', (req,res)=>{
    Cupon.create({
        status:req.body.status,
        dataInceput:req.body.dataInceput,
        dataExpirare:req.body.dataExpirare,
        discount:req.body.discount,
        unitateDiscount:req.body.unitateDiscount,
        observatii:req.body.observatii,
        username:req.body.username,
        idCompanie:req.body.idCompanie
    }).then((cupon)=>{
        res.status(200).send("Cuponul a fost creat cu succes!");
    },(err)=>{
        res.status(500).send(err);
    });
});

//afisarea tuturor cupoanelor
app.get('/toate-cupoanele', (req,res)=>{
    Cupon.findAll().then((cupon)=>{
        res.status(200).send(cupon);
    });
});

//stergere cupoane 
app.delete('/stergere-cupon/:idCupon', (req,res)=>{
    const idCupon=req.params.idCupon;
    Cupon.destroy({
        where: {
            idCupon: idCupon
        }
    }).then((cupon)=>{
        res.status(200).send('Cuponul a fost sters cu succes!');
    },(err)=>{
        res.status(500).send(err);
    }
    );
});

//actualizare cupon
app.put('/actualizare-cupon/:idCupon', (req,res)=>{
    const idCupon=req.params.idCupon;
    Cupon.update({
        status: req.body.status,
        //dataInceput: req.body.dataInceput,
        dataExpirare: req.body.dataExpirare,
        discount: req.body.discount,
        unitateDiscount: req.body.unitateDiscount,
        observatii: req.body.observatii,
        userUsername:req.body.username,
        companieIdCompanie:req.body.idCompanie
    },
    {
        where: {
            idCupon: idCupon
        }
    }).then((cupon)=>{
        res.status(200).send("Cuponul a fost actualizat cu succes!");
    }, (err)=>{
        res.status(500).send(err);
    });
});

//Metode ptr COMPANII si LOCATII

//adaugare companie
app.post('/adaugare-companie', (req, res) =>{
    Companie.create({
        denumire: req.body.denumire,
        tipCompanie: req.body.tipCompanie
    }).then((companie) => {
        res.status(200).send(companie);
    }, (err) => {
        res.status(500).send(err);
    })
});

//preluare companii cu o anumita denumire
app.get('/companii-denumire/:denumire', (req, res) => {
    const denumire = req.params.denumire;
    Companie.findAll({where:{denumire: denumire}}).then((companii) => {
        res.status(200).send(companii); 
    });
});


app.get('/companii', (req, res) => {
	try{
		Companie.findAll().then((companie)=>{
        res.status(200).send(companie);
	});}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
});

//id maxim companie
app.get('/id-maxim-companie', (req, res) => {
    try{
		Companie.max('idCompanie').then((idCompanie)=>{
        res.status(200).send(idCompanie);
	});}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
});

app.post('/companii', (req, res) => {
	try{
		
		Companie.bulkCreate(req.body);
		res.status(201).json({message : 'created'});
	
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
});

app.get('/companii/:id', (req, res) => {
	try{
		let companie = Companie.findByPk(req.params.id).then((companie)=>{
        res.status(200).send(companie)});
        
	}
	catch(e){
		console.warn(e);
		res.status(500).json({message : 'server error'});
	}
});

app.put('/companii/:id', (req, res) => {
     Companie.update(
         {
             denumire: req.body.denumire,
             tipCompanie: req.body.tipCompanie,
             
         },
         {
             where: {
                 idCompanie: req.params.id
             }
         })
         .then((user) => {
             res.status(200).send("Actualizarea s-a efectuat cu succes!");
         }, (err) => {
            res.status(500).send(err);
         });

    });

app.delete('/companii/:id', (req, res) => {
	try{
		  Companie.destroy(
		      {
		          where : {
		              idCompanie: req.params.id
		          }
		      });
		  res.status(202).json({message : 'accepted'})
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
});

app.post('/locatii', (req, res) => {
	try{
		
		Locatie.bulkCreate(req.body);
		res.status(201).json({message : 'created'});
	
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
});

//adaugare locatie
app.post('/companii/:idCompanie/locatii', (req, res) => {
 Locatie.create({
        //idLocatie: req.params.idLocatie,
        adresa: req.body.adresa,
        idCompanie:req.params.idCompanie
    }, {where: {
        idCompanie : req.params.idCompanie
    }}).then((user) => {
        res.status(200).send("Locatia a fost creata cu succes!");
    }, (err) => {
        res.status(500).send(err);
    })
});


app.get('/locatii', (req, res) => {
	try{
		Locatie.findAll().then((locatie)=>{
        res.status(200).send(locatie);
	});}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
});


app.get('/companii/:id/locatii', (req, res) => {
    try{
		Locatie.findAll({where:{idCompanie: req.params.id}}).then((locatie)=>{
        res.status(200).send(locatie);
	});}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
});

app.get('/companii/:idCompanie/locatii/:idLocatie', (req, res) => {
    	try{
		let locatie = Locatie.findByPk(req.params.idLocatie, {where:{idCompanie:req.params.idCompanie}}).then((locatie)=>{
        res.status(200).send(locatie)});
        
	}
	catch(e){
		console.warn(e);
		res.status(500).json({message : 'server error'});
	}
});

app.post('/companii/:idCompanie/locatii/:idLocatie', (req, res) => {
 Locatie.create({
        idLocatie: req.params.idLocatie,
        adresa: req.body.adresa,
        idCompanie:req.params.idCompanie
    }, {where: {
        idCompanie : req.params.idCompanie
    }}).then((user) => {
        res.status(200).send("Locatia a fost creata cu succes!");
    }, (err) => {
        res.status(500).send(err);
    })
});

app.put('/companii/:idCompanie/locatii/:idLocatie', (req, res) => {
	Locatie.update(
         {
             adresa: req.body.adresa,
             idCompanie: req.body.idCompanie,
             
         },
         {
             where: {
                 idCompanie: req.params.idCompanie,
                 idLocatie: req.params.idLocatie
             }
         })
         .then((user) => {
             res.status(200).send("Actualizarea s-a efectuat cu succes!");
         }, (err) => {
            res.status(500).send(err);
         });
});

app.delete('/companii/:idCompanie/locatii/:idLocatie', (req, res) => {
		try{
		  Locatie.destroy(
		      {
		          where : {
		              idCompanie: req.params.idCompanie,
		              idLocatie: req.params.idLocatie
		          }
		      });
		  res.status(202).json({message : 'accepted'})
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
});


app.listen(8081, ()=>{
    console.log('Server started on port 8081...');
});

